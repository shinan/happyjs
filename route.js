const DocController = require('./server/module/documnet-module');
module.exports = [
    {
        path: '/api/ReadWrite',
        method: 'POST',
        handler: DocController.create
    },
    {
        path: '/api/GetDoc',
        method: 'POST',
        handler: DocController.find 
    }
];