'use strict';

const fs = require('fs');

module.exports = {

    async create(req, reply) {


        let rawdata = await fs.readFileSync('file.json');
        let student = await JSON.parse(rawdata);
        let keys = Object.keys(student);

        let len = keys.length

        let element=[]
        for (let i = 0; i < len; i++) {
            let arr=student[i]
            arr.forEach(data => {
                element.push(data);

            });

        }

        let fun = await list_to_tree(element)
        let data = JSON.stringify(fun);
        fs.writeFileSync('Arranged.json', data);


         return reply.response({message:"Success",result:fun})



    },
    async find(req, reply) {


        let rawdata = await fs.readFileSync('file.json');
        let student = await JSON.parse(rawdata);
        let keys = Object.keys(student);

        let len = keys.length

        let element=[]
        for (let i = 0; i < len; i++) {
            let arr=student[i]
            arr.forEach(data => {
                element.push(data);

            });

        }

        let current_page=req.payload.current_page
        let per_page_items=req.payload.per_page_items

        let fun = await paginator(element,current_page, per_page_items)

        return reply.response({message:"Success",result:fun})


        
    },


};


const list_to_tree= async function (list) {

    let map = {}, node, roots = [], i;
    
    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i; // initialize the map
      list[i].children = []; // initialize the children
    }
    
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      if (node.parent_id !==null) {
        list[map[node.parent_id]].children.push(node);
      } else {
        roots.push(node);
      }
    }
    return roots;
  }


  const paginator= async function (items, current_page, per_page_items) {
    let page = current_page || 1,
        per_page = per_page_items || 10,
        offset = (page - 1) * per_page,

        paginatedItems = items.slice(offset).slice(0, per_page_items),
        total_pages = Math.ceil(items.length / per_page);

    return {
        page: page,
        per_page: per_page,
        pre_page: page - 1 ? page - 1 : null,
        next_page: (total_pages > page) ? page + 1 : null,
        total: items.length,
        total_pages: total_pages,
        data: paginatedItems
    };
}